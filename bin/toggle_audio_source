#!/bin/bash

set -o errexit -o nounset -o pipefail

# two arrays: sink descriptions, sink names
# select from sink descriptions, get name from assuming same index

DEFAULT_SINK="$(pactl get-default-sink)"
SINKS_INFO="$(pactl --format json list sinks)"

readarray -t sink_names < <(jq --raw-output '.[].name' <<< "$SINKS_INFO")
# append a '*' to the description of the default sink
readarray -t sink_descs < <(\
    pactl --format json list sinks |
    jq --raw-output --arg default "$DEFAULT_SINK" \
        '.[] | (if .name==$default then "* \(.description)" else .description end)'\
    )

select desc in "${sink_descs[@]}"
do
    if [[ -n "$desc" ]]
    then
        break
    fi

    echo "invalid selection $REPLY" >&2
done

# assume sink_names and sink_descs are in the same order
pactl set-default-sink "${sink_names[$(( REPLY - 1 ))]}"
