# Arch on ThinkPad P14s Gen2 (AMD)

With full disk encryption via LVM. Haven't tried to get the fingerprint sensors
working because I don't use it.

## Recovery

Decrypt and mount the system

```
$ cryptsetup open /dev/nvme0n1p2 cryptlvm
$ mount /dev/VolGroup/root /mnt
$ mount /dev/nvme0n1p1 /mnt/boot
$ arch-chroot /mnt
```

## Packages

Just a note on why some packages exist:

```
# tlp suggestions
acpi_call
smartmontools
ethtool
# end
# spotify extras
zenity
# end
```

## User Configuration

Add myself to `video` group for backlight control, see
`etc/udev/rules.d/backlight.rules`. Also add myself to `audio` group for
control of mute/micmute LEDs, see `etc/udev/rules.d/kdb_led.rules`


```
usermod --append --groups video,audo mjh
```

## Dotfiles Configuration

First time creation:

```
$ git init --bare $HOME/.dotfiles/dotfiles
$ git --git-dir=$HOME/.dotfiles/dotfiles --work-tree=$HOME config --local status.showUntrackedFiles no
```

Cloning:

```
$ git init --bare "$HOME/.dotfiles/dotfiles"
$ git --git-dir="$HOME/.dotfiles/dotfiles/" remote add origin <remote>
$ git --git-dir="$HOME/.dotfiles/dotfiles/" --work-tree="$HOME" pull origin master
```

## Audio Configuration

```
$ pulseaudio --check
$ pulseaudio -D
```

## Backlight

Adjusting the backlight works fine using the `udev` rule above, however
`systemd` wants to start `systemd-backlight@backlight:acpi_video0.service`
regardless which will fail each time
(`https://wiki.archlinux.org/title/Lenovo_ThinkPad_T14_(AMD)_Gen_1#Backlight`),
so just mask it

```
# systemctl mask systemd-backlight@backlight:acpi_video0.service
```
